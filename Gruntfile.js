module.exports = function (grunt) {
    const sass = require('node-sass');
    require('time-grunt')(grunt);
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemmin'
    })
    grunt.initConfig({
        sass: {
            dist: {
                files: [{
                    expand: true,
                    cwd: 'css',
                    src: ['*.scss'],
                    dest: '/dist',
                    ext: '.css'
                }]
            },
            options: {
                implementation: sass
            },
        },
        watch: {
            files: ['css/*.scss'],
            tasks: ['css']
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src: [
                        'css/*.css',
                        '*.html',
                        'js/*.js'
                    ]
                },
                options: {
                    watchTask: true,
                    server: {
                        baseDir: './' //Directorio base para el servidor
                    }
                }
            }
        },
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd:'./',
                    src: 'images/*.{jpg, png, gif, jpeg}',
                    dest: 'dist/'
                }]
            }
        },
        copy: { //pasa los archivos html a la carpeta dist
            html: {
                files: [{
                    expand: true,
                    dot: true,
                    cwd:'./',
                    src: ['*.html'],
                    dest: 'dist'
                }]
            },
            fonts: {
                files: [{
                    //for font-awasome
                    expand: true,
                    dot: true,
                    cwd: 'node_modules/open-iconic/font',
                    src: ['fonts/*.*'],
                    dest: 'dist'
                }]
            },
            css: {
                files: [{
                    //for font-awasome
                    expand: true,
                    dot: true,
                    cwd:'./',
                    src: ['css/*.*'],
                    dest: 'dist'
                }]
            }     
        },
     
        clean: { // elimina la carpeta dist
            build: {
                src: ['dist/']
            }
        },

        cssmin:{//css minificador
            dist:{}
        },

        uglify:{//js minificador
            dist:{}
        },

        filerev: {
            options: {
                encoding: 'utf8',
                algorithm:' md5',
                lenght: 20
            },
            release:{
                //filerev:release hashes(md5) all assets (images,js and css)
                //in dist directory
                files: [{
                    src:[
                        'dist/js/*.js',
                        'dist/css/*.css',
                    ]
                }]
            }
        },

        concat:{
            options:{
                separador:';'
            },
            dist:{}
        },

        useminPrepare:{
            foo: {
                dest:'dist',
                src: ['index.html', 'about.html', 'contact.html', 'prices.html']
            },
            options: {
                flow: {
                    steps:{
                        css: ['cssmin'],
                        js: ['uglify']
                    }
                },
                post: {
                    css: [{
                        name: 'cssmin',
                        createConfig: function(context, block){
                            var generated = context.options.generated;
                            generated.options = {
                                keepSpecialComments:0,
                                rebase: false
                            }
                        }
                    }]
                }
            }
        },

        usemin: {
            html:['dist/index.html', 'dist/about.html', 'dist/contact.html', 'dist/prices.html'],
            options:{
                assetsDir:['dist', 'dist/css', 'dist/js']
            }
        }

    });
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-usemin');
    grunt.loadNpmTasks('grunt-filerev');
    grunt.loadNpmTasks('grunt-sass');
    
    grunt.registerTask('css', ['sass']);
    grunt.registerTask('default', ['browserSync', 'watch']);
    grunt.registerTask('img:compress', ['imagemin']);
    grunt.registerTask('build', [
        'imagemin',
        'copy',
        'imagemin',
        'useminPrepare',
        'concat',
        'cssmin',
        'uglify',
        'filerev',
        'usemin'
    ]);

    // grunt.registerTask('concat-js', ['concat:js']);
    // grunt.registerTask('concat-css', ['concat:css']);
}