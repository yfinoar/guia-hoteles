$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval: 2000
    });
    $('#exampleModal').on('show.bs.modal', function(e){
        console.log('El modal se esta mostrando')
    })
    $('#exampleModal').on('shown.bs.modal', function(e){
        console.log('El modal se mostró')
        $('.btnContacto').prop('disabled', true)
    })
    $('.btnContacto').removeClass('btn-outline-success')
    $('.btnContacto').addClass('btn-outline-danger')

});